
# Getting Started with Oracle Cloud Infrastructure

## OCI Architecture


* Regions 
  + ***localized geographic area*** that is comprised of one or more availability domains (ADs)
  + ***physical data centers*** located within that region.

* Availability Domains (ADs) W
  + ADs connected - low latency / high bandwidth network
  
  ![](images/01_02_01_ADs.png)
  * Isolated form each other, fault tolerant
  * Physical Infrastructure is NOT share  
  
* Fault Domains (FDs)

  + Grouping of **hardware and infrastructure** within each AD
  + distribute your cloud resources => not running on SAME physical hardware within AD => avoid single point of failure.
    + protect against any unexpected hardware failure or even against planned outages that can occasionally occur during compute hardware maintenance.

  ![](images/01_02_01_ADs.png)
  + Each AD have 3 FDs
  + FDs act as logical **data center** within an AD
  + Resource placed in different FDs will notshare single points of hardware failure.
  
  
### Choosing a region  
1. Location - Choose a region closest to your user => lower latency and higest performance
2. Data Residency & Compliance - Mandy countries have strict data Residency & Compliance
3. Service Availabilty - some services are not offered everywhere (e.g. New clound services made available based on regional demand)



## OCI Compute Service

* Manage - Compute Instances. ie. Servers
* Foundation for other OCI services
  + Create and instance
  + Access security
  + Install an application
* Based on shape
  + CPU
  + memory
  + Storage  

* Types of compute
  + Virtual Machine
  + Bare Metal 
   - Dedicated "physical-server-access"
  + Dedicateds VM Host  
   - Run VMs on **dedicated** server
  
* Shapes  
  + a template that determines the **resource** allcated to an instance
  + AMD, Intel and Arm-based processors
  + Types 
    + Fixed shape - Cannot be customzed Bare Metal or VMs
	+ Flexible Shapre = Can be customized (i.e. change no. of CPUs)
	
![](images/01_03_02_shapes.png)	
	
* Verical Scaling
  + change the shape
  + note: stop inatnace before resizing
  
  
![](images/01_03_03_vertial_scaling.png)	
  
* Autoscaling
  + Enables large-scale deployment of VMs
  + Scale out or Sacle in
  + Metric-based autoscaling
    + Choose a performance metric to monitor
	+ set threshold for that metric
	+ autoscaling event will trigger when threshold is met
  + Schedule-based autoscaling
    
  
  
  
  
	
	
	
  
