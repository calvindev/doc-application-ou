# Identtity and Access Management (IAM) 

* IAM - Identity and access Managemnet Service
* Fine-grained Access Control - Role Based Access Control
* AuthN - Who are you?
* AuthZ - what permission do you have?

## IAM introduction

### OCI Idenity concepts 
* users 
* principles 
* groups
* policies
* compartments
* resources.

* Identiy Domain 
  + Represent a user population in OCI and associated configuration and security settings.  
  ![Idenity Domain](images/04_01_0030_identityDomain.png)

* OCI Identity Concepts
  ![OCI Identity Concepts](images/04_01_0040_identityConcept.png)

### OCI Resource 

* Unique Orcale-assigned identifier
* Oracle Cloud ID (OCID) = `ocid1.<Resource Type>.<Realm>.[Region][.Future Use].<Unique Id>`
  - ocid1: type of Resource
  - Resource Type: e.g. Compute Instance, Block Storage
  - Realm: Set of Regions That Share Same Characteristics
  ![Idenity Domain](images/04_01_0050_resource.png)


## Compartment 

### Overview
* Tenacy (Root Compartment) 
  * Compartment Network 
  * Compare Storage

* Compartment - "group"
  * collection of related reousrces
  * Isolate and control access

* Best practice : Create dedicated compartments to isolate resources

  ![Compartment](images/04_01_0100_compartment.png)

### Resource Compartments

![Compartment](images/04_01_0110_compartment_resource.png)

* Each resource belongs to a single compartment
*( Access: Groups + Policies = Access to Compartments

  ![Compartment](images/04_01_0120_compartment_access.png)   
 
* Can
  * **Interaction** of Resource
    * Resource can interact with other res in different compartments
   ![**Interaction** of Resource](images/04_01_0130_interaction.png)
    
  * **Movement** of Resource
    * Resource can be moved from one compartment to another
   ![Movement of Resource](images/04_01_0140_movement.png)

   * Multipple Regions - Resource from multiple regions can be in SAME compartment
   ![Movement of Resource](images/04_01_0150_multipleRegion.png)
   
* Nested Comparement - 6 levels
  ![Nested](images/04_01_0160_nested.png)

* Set Quotas / Budgets on Compartments
  ![Bugdet](images/04_01_0170_budget.png) 
   
## XXX: AuthN / AuthZ 
* Principals


AuthZ - Authentication 
1. User Username/password
2. API Signing Key - public / private key
3. Auth Token 

AuthZ - What permission?
* AuthZ in OCI - IAM '''Policies'''
* Grant List - Allow `group_name` to `verb` `resource-type` in `loclation` where `condition`
  *Verb - Manage / Use / Read / Inspect

## Networking

### Virtual Cloud Network (VCN) 

VCN
* Private Network / Secure Communication / Lives in OCI Region
* HA / Scalable / Secure

VCN Address Space

Communication -- Internet
* Internet Gateway
* NAT Gateway
* Service Gateway - access OCI public service

Communication -- On-Premises

### Routing 
* Route Table
* Peer - Local / Remote