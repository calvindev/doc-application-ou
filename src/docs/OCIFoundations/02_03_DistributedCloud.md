# OCI Distributed cloud

## Offers
1. Public cloud
2. Hybird cloud
3. Dedicated cloud
4. Multi cloud

![](images/02_03_0020.png)

## Hybird cloud
1. Dedicated Region Cloud@Customer
2. Oracle Cloud VMware Solution
3. Autonomous DB on Exadata Cloud@Customer
4. Roving Edge Infrastructure

![](images/02_03_0030.png)

![Dedicated Region Cloud@Customer](images/02_03_0030a.png)

![Azure Interconnect](images/02_03_0030b.png)
 
![Oracel DB Servuce for Azure](images/02_03_0040.png)

 