# Architecture

* Architecture
 ![overview](images/02_02_0010.png)
  * Regions - Localized Geographic Area
  * Availability Domians (AD)
    * fault tolerant
    * Within a region
    * Low Latency / High bandwidth
  * Fault Domain (FD)
    * gorup of "Hardware + Infrastructure"
    * logical datacenters
  
 
* Choosing a region
  ![overview](images/02_02_0020.png)
  1. Location - choose a region closet to your users for lowest latency and highest performance!
  2. Data Resisdency & Compliance - have strict data residency requirement
  3. Service Availability
     + New cloud serrvie - regional demand , regulatory compliance, resource availability, and other factors
  
  
* Availability Domians (AD) 
  ![AD](images/02_02_0030_AD.png)
  + Isolated from eacho other, fault tolerant  
  + Physsical infrastructe not shared (e.g. power)
  
* Fault Domain (FD)
  
  ![FD](images/02_02_0040_FD.png)
  + e.g. AD = 3x FD
  + Logical data center within AD
  + Resources placed in diff FDs will not share single points of hardware failure
  
  * in any region - at most ONE FD are begin actively changed at any point of time.
    ![FD](images/02_02_0041_FD.png)
   
* Avoid "Sinlge point of failure"   

  * deploy instances that perform the same tasks
    * in ONE AD regions -  in diff FDs
	* in diff ADs for multiple AD regions 
	
  ![Fx](images/02_02_0050.png)

  ![Fx](images/02_02_0051.png) - for Single AD
  
* High Availability 
  ![A](images/02_02_0060.png)
  * FDs
  * ADs
  * Region Pair
  