  


# OCI Architecture 
Avoid Single point of falure

* Region Pair - for Recover
* Availability Domains - Isolated from each other [Region]
  
* Fault Domains - AD has 3 Fault Domains(FD).
# Oracle Cloud Free Tier Account 

## OCI Console walk-through 

* Web Interface to management OCI

### Tenancy ###

Root Compartment
* Users - Tenancy ADmin
* Groups - Adminstrators
* Policies - allow grouips admin to manage all resources in tenenancy

Best Practice:
*Compartment: isolated resource

* Don't use the Tenancy Admin for daily operations
* Enfore the use of Multi-factor authentication (MFA)

# Identtity and Access Management (IAM) 

* IAM - Identity and access Managemnet Service
* Fine-grained Access Control - Role Based Access Control
* AuthN - Who are you?
* AuthZ - what permission do you have?

concepts 
* users
* principles 
* groups
* policies
* compartments
* resources.

OCI Resource 
* Oracle Cloud ID (OCID) = ocid.<Resource Type>.<Realm>.[Region][.Future Use].<Unique Id>

## AuthN / AuthZ 
* Principals


AuthZ - Authentication 
1. User Username/password
2. API Signing Key - public / private key
3. Auth Token 

AuthZ - What permission?
* AuthZ in OCI - IAM '''Policies'''
* Grant List - Allow <group_name> to <verb> <resource-type> in <loclation> where <condition>
**Verb - Manage / Use / Read / Inspect

## Compartment 
* Tenacy (Root Compartment) > Compartment Network | Compare Storage
* Compartment -- "group"
** collection of related reousrces
** Isolate and control access

Best practice : Create dedicated compartments to isolate resources

* Each resource belongs to a single compartment
*( Access: Groups + Policies = Access to Compartments
* '''Interaction''' of Resrouce 
** Res can interact with other res in different compartments
* '''Movement''' 
* Multipple Regions - Res from multiple regions can be in SAME compartment
* Nested Comparement - 6 levels
* Set Quotas / Budgets on Compartments

## Networking

### Virtual Cloud Network (VCN) 

VCN
* Private Network / Secure Communication / Lives in OCI Region
* HA / Scalable / Secure

VCN Address Space

Communication -- Internet
* Internet Gateway
* NAT Gateway
* Service Gateway - access OCI public service

Communication -- On-Premises

### Routing 
* Route Table
* Peer - Local / Remote




##
https://www.reddit.com/r/oraclecloud/comments/14pg5dr/oracle_always_free_service_have_boot_volume_cost/